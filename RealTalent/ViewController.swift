//
//  ViewController.swift
//  RealTalent
//
//  Created by Kvana Inc 1 on 22/06/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var userNameTxtF: UITextField!
    @IBOutlet weak var passwordTxtF: UITextField!
    
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    override func viewWillAppear(animated: Bool) {
       self.navigationController?.navigationBarHidden = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signInBtnClicked(sender: AnyObject) {
        let talentView : TalentListViewController = storyboard?.instantiateViewControllerWithIdentifier("TalentListViewController") as! TalentListViewController
        self.navigationController?.pushViewController(talentView, animated: true)
        }
    
    @IBAction func signUpBtnClicked(sender: AnyObject) {
    }
    override func viewWillDisappear(animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
  }

