//
//  TalentListViewController.swift
//  RealTalent
//
//  Created by Kvana Inc 1 on 22/06/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class TalentListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var talentListTable: UITableView!
    
    var  talentArray = ["DanceSchools","EducationalInstitutes","MusicClasses", "FilmInstitutes", "SangeethamClasses", "Motor-Driving","RealEstates","YogaClasses","ChildCareCenters","Old Age Homes","OrphansCenter"]
    var  colorsArray = ["#43ab7d","#21afdc", "#62c814", "#a9a9a9", "#2169dc", "#008b8b", "#3ae3e3","#5f9ea0","#8f7a8d","#7853c2","#5f9ea0","#2169dc"]
    var  talentImagesArray = ["dance.png","music.png", "mimicry.png", "sing.png", "coock.png", "drawing.png", "write","painting","story","talent-1","write","painting"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
          }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
                   return talentArray.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellReuseIdentifier = "TalentListTableCell"
        let cell:TalentListTableCell = talentListTable.dequeueReusableCellWithIdentifier(cellReuseIdentifier) as! TalentListTableCell!
         cell.talentIcon.image=UIImage(named: talentImagesArray[indexPath.row])
          cell.talentName.text = talentArray[indexPath.row]
          cell.talentName.textColor=UIColor .whiteColor()
          return cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        cell.backgroundColor=UIColor.init(rgba: colorsArray[indexPath.row])
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let talentView : VideoViewController = storyboard?.instantiateViewControllerWithIdentifier("VideoViewController") as! VideoViewController
        self.navigationController?.pushViewController(talentView, animated: true)
        
    }
}


