//
//  VideoViewController.swift
//  RealTalent
//
//  Created by Kvana Inc 1 on 04/07/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit
import Social

class VideoViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {
 let imagePicker = UIImagePickerController()
    @IBOutlet weak var img: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        imagePicker.delegate = self
        // Create the alert controller
        let alertController = UIAlertController(title: "ChooseVideo", message: nil, preferredStyle: .Alert)
        
        // Create the actions
        let uploadVideoAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "UploadVideo", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            self.imagePicker.allowsEditing = false
            self.imagePicker.sourceType = .PhotoLibrary
           self.presentViewController(self.imagePicker, animated: true, completion: nil)
            
        }
        alertController.addAction(uploadVideoAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
}
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!) {
        let selectedImage : UIImage = image
        //var tempImage:UIImage = editingInfo[UIImagePickerControllerOriginalImage] as UIImage
        img.image=selectedImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            img.contentMode = .ScaleAspectFit
         img.image = pickedImage
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnShareClicked(sender: AnyObject) {
        
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
//            var facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
//            facebookSheet.setInitialText("Share on Facebook")
            let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            vc.setInitialText("Look at this great picture sending from swift xcode !")
            vc.addImage(img.image!)
          //  vc.addURL(NSURL(string: "http://www.photolib.noaa.gov/nssl"))
            presentViewController(vc, animated: true, completion: nil)
            //self.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            var alert = UIAlertController(title: "Accounts", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
    }
}
