//
//  TalentListTableCell.swift
//  RealTalent
//
//  Created by Kvana Inc 1 on 22/06/16.
//  Copyright © 2016 Kvana. All rights reserved.
//

import UIKit

class TalentListTableCell: UITableViewCell {

    @IBOutlet weak var talentIcon: UIImageView!
    @IBOutlet weak var talentName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
  }
